Gumichan01 — I am just chilling
Genre: Lo-fi
Tempo: 80 BPM
Rythm : 4/4

This song is under Creative Commons Attribution-ShareAlike 4.0
CC-BY-SA 4.0

https://creativecommons.org/licenses/by-sa/4.0/


Samples:

- open-Window / close-window — CC-BY

Derivative work of BarkersPinhead — Open and Close Window — CC-BY
https://freesound.org/people/BarkersPinhead/sounds/274809/

- vinyl-15-sec — CC-BY

Derivatibe work of waveplay — ~30 Sec. Vinyl Ambience — CC-BY
https://freesound.org/people/waveplay./sounds/383224/

- fr-streetrain pt.1 and 2 — CC-BY

Derivatibe work of Kevin Luce — LS_34553_FR_StreetRain — CC-BY
https://freesound.org/people/kevp888/sounds/517665/

- Insert-cd (Original work) — CC-BY

- Alys-chorus-melody-1-D4 (Original work) — CC-BY

Made by using Alys and Alter/Ego.
Alys is a virtual singer created by Voxwave.
Alter/Ego is a real time singing synthesizer made by Plogue.

- Cymatics - Lofi Melody Loop 6 - 80 BPM G Min
This sample belongs to Cymatics. I took it from https://cymatics.fm/products/lofi-toolkit


> NB 1

This project uses some free VST plugins:

- GComp: Compressor (Used in the vocal channel)
- GMulti: Multi-band compressor. I use it for the mastering process
- Izotope Vinyl: A free vinyl plugin. I use it only for the spin-down effect on the vinyl sound at the end.
- Voxengo SPAN: spectrum analyzer

Thos plugins are optional. You still can play the song normaly.

> NB 2

This project originally use Alter/Ego for the chorus voice in the last part of the sound.
Even if Alter/Ego is free, the voice bank I used is not and it is Licence-protected. So I cannot provide it.
However, I sampled the voice part (Alys-chorus-melody-1-D4.wav)