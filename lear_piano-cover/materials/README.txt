nora2r � Lear (RawDraft Piano Cover)
Genre: Piano / Instrumental

This cover is under Creative Commons Attribution 4.0
CC-BY 4.0

https://creativecommons.org/licenses/by/4.0/


## Disclaimer ##

/!\ Disclaimer: The original song belong to its owner: nora2r /!\

Original song: https://youtu.be/fOLeoTWKD6I

This song is from this compilation: https://notebookrecordsjp.bandcamp.com/album/storyteller-volume-3

If you like the original song, please support him:

- https://soundcloud.com/nora2r
- https://www.youtube.com/channel/UCVM2BfXE7MUtfQbfvqTmS_Q
- https://nextreflection.bandcamp.com/


## (o.o) ##

I made a cover of the melodic part of the song, that is why the cover is shorter than the original song


## Materials ##

- Piano.sf2

This project only uses the piano soundfont (Piano.sf2).
This soundfont is under Creative Commons Attribution 4.0
https://creativecommons.org/licenses/by/4.0/

This project uses two external plugins:

- [Stereo Enhancer](http://tonebytes.com/stereo-enhancer/) for stereo to mono conversion on the chords  
- [TDR Kotelnikov](https://www.tokyodawn.net/tdr-kotelnikov/) for mastering compression

Those plugins are not mandatory. You can use this project without them.
