Gumichan01 — Évasion Onirique (Instrumental)
Genre: Drum and Bass
Tempo: 170 BPM
Time Signature: 4/4

This song is under Creative Commons Attribution 4.0
CC-BY 4.0

https://creativecommons.org/licenses/by-sa/4.0/


This song is a vocal-ready instrumental sound. Free free to put your voice on it.


> Technical Information

Every samples I used are under CC0, except those from Cymatics.

I used a soundfont for the "Lead Y Piano" and [Helm](https://tytel.org/helm/) for the "Dark bass". Those two instruments are sampled.
Those samples are under CC0.


This project uses some free VST plugins:

- GComp: Compressor - Freeware
- DragonFly Plate Reverb: Reverb plugin - Free (as in freedom) and open-source
- TDR Kotelnikov: Compressor - Freeware
- TDR Nova: Multiband EQ/Compressor. I use it for multiband compression - Freeware
- Voxengo SPAN: spectrum analyzer - Freeware

Those plugins are optional.


> Comment

This is not my first Drum and Bass work [1][], but this new work is a bit more complex than the first one.
The big challenge here was to integrate a sample (made for Lo-fi music originally) in a complete Drum and bass sound.
I spend ~3 weeks writing and designing this song and ~2 days mixing and mastering it.


---

1: See https://soundcloud.com/gumichan01/limbo
