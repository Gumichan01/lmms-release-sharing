Gumichan01 — Boussole mentale
Genre: Ambient
Tempo: 78  BPM
Time Signature: 2/4

Inspired by "Courtney_Hawkins_30 Day Beat Challenge [Week 3]"
- https://youtu.be/gniBlEayrhM?t=148

This song is under Creative Commons Attribution-ShareAlike 4.0
CC-BY-SA 4.0

https://creativecommons.org/licenses/by-sa/4.0/
