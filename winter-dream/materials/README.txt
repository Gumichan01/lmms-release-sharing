Gumichan01 — Winter Dream
Genre: Electronic
Tempo: 130 BPM
Rythm : 4/4

This song is under Creative Commons Attribution-ShareAlike 4.0
CC-BY-SA 4.0

https://creativecommons.org/licenses/by-sa/4.0/


Samples:

- Yaiyai (Gumichan01 edit) - CC-BY

Derivative work of BazzBoy — yaiyai — CC0. https://freesound.org/people/bazz_boy/sounds/335216/ 

- BASS dubstep — CC0

- Cymatics samples from Cymaics PHARAO beta pack. https://cymatics.fm/pages/pharaoh-beta-pack

They are under copyright, but honestly I don't give a fuck of that.

- gumi-chip-drop-helm-sample - CC-BY

- Growl - CC0

- piano-lead-sample0 - CC0



Preset:

- Gumi-chip-drop_helm - CC-BY

This is an Helm preset. You can use the sample version instead if you don't have Helm.

> NB 1

This project uses some free VST plugins, including Helm, but they are optional. You still can play the song normaly.